require 'json'
require 'sinatra'
require 'sinatra/reloader' if development?
require 'sinatra/cookies'
require 'adafruit/io'

ADAFRUIT_IO_USERNAME = 'crossditada2020'
ADAFRUIT_IO_KEY = 'aio_ileM209iqZv6OFRp6inZ7cSmGVcz'
HUMIDITY_THRESHOLD = 80

# create an instance
aio = Adafruit::IO::Client.new(
  key: ADAFRUIT_IO_KEY,
  username: ADAFRUIT_IO_USERNAME
)

options '*' do
  headers 'Allow' => 'HEAD,GET,PUT,POST,DELETE,OPTIONS'
  headers 'Access-Control-Allow-Headers' => 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept'
end

get '/start' do
  headers 'Access-Control-Allow-Origin' => '*'

  current_humidity = aio.data('humidity').map{|x| x['value'].to_f}.first
  JSON.dump message: "Cleaning started #{Time.now}", time: Time.now.to_s, humidity: current_humidity
end

post '/done' do
  headers 'Access-Control-Allow-Origin' => '*'

  if params[:start].nil?
    return JSON.dump message: "Error: you must supply a start time, e.g. start=#{Time.now}"
  end

  start = Time.parse(params[:start])
  now = Time.now
  data = aio.data('humidity').reject do |x|
    created_at = Time.parse(x["created_at"])
    created_at < start || created_at > now
  end
  max = data.map{|x| x['value'].to_f}.max
  current_humidity = aio.data('humidity').map{|x| x['value'].to_f}.first
  if max.nil?
    JSON.dump clean: false, reason: 'Du har klikket videre for hurtigt, vi har ikke nået at modtage noget sensor data siden du startede (prøv igen og vent ca. 10 sekunder mellem start og slut af rengøring)', max_humidity: max, current_humidity: current_humidity, threshold: HUMIDITY_THRESHOLD
  elsif max > HUMIDITY_THRESHOLD
    JSON.dump clean: true, max_humidity: max, current_humidity: current_humidity, threshold: HUMIDITY_THRESHOLD
  else
    JSON.dump clean: false, max_humidity: max, current_humidity: current_humidity, threshold: HUMIDITY_THRESHOLD
  end
end

# data = new URLSearchParams();
# data.append('start', new Date(new Date() - 1000000).toISOString())
# fetch('https://crossdit-2020.herokuapp.com/done', {method: 'post', body: data})
#   .then(response => response.json())
#   .then(data => console.log(data));
